/* 
 * 
 * Author Ryan Guarascia
 * Class COMP-10062-03-W16-NC
 * Date: March 10 2016
 * Description: Calculates the employee's payment and their employee number
 * Ryan Guarascia, student number: 000379166, certify that this material is my original work. No other person's work has been used without due acknowledgement and I have not made my work available to anyone else.
 */
package lab3a;

import java.util.Scanner;

/**
 * Main lab document
 *
 * @author rguarascia
 */
public class Lab3A {

    /**
     * Main method. Gets employee's name and display their employee id and their payment
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner io = new Scanner(System.in);

        //Creates all the objects
        System.out.print("Enter a name: ");
        Employee Emp1 = new Employee(io.next());
        Emp1.setEmployeePay(22.50, 35.0);
        System.out.printf("Employee count is %d \n", Emp1.getCount());

        System.out.print("Enter a name: ");
        Employee Emp2 = new Employee(io.next());
        Emp2.setEmployeePay(45350.00);
        System.out.printf("Employee count is %d \n", Emp2.getCount());

        System.out.print("Enter a name: ");
        Employee Emp3 = new Employee(io.next());
        Emp3.setEmployeePay(500.00, 25);
        System.out.printf("Employee count is %d \n", Emp3.getCount());

        System.out.print("Enter a name: ");
        Employee Emp4 = new Employee(io.next());
        Emp4.setEmployeePay(14.75, 48.0);
        System.out.printf("Employee count is %d \n\n\n", Emp4.getCount());

        //Final output chart
        System.out.println(String.format("Employee %d %s earned: %.2f", Emp1.getNumber(), Emp1.getName(), Emp1.calculatePay()));
        System.out.println(String.format("Employee %d %s earned: %.2f", Emp2.getNumber(), Emp2.getName(), Emp2.calculatePay()));
        System.out.println(String.format("Employee %d %s earned: %.2f", Emp3.getNumber(), Emp3.getName(), Emp3.calculatePay()));
        System.out.println(String.format("Employee %d %s earned: %.2f", Emp4.getNumber(), Emp4.getName(), Emp4.calculatePay()));
    }

}
