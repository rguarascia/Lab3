/* 
 * Author Ryan Guarascia
 * Class COMP-10062-03-W16-NC
 * Date: March, 9th, 2016
 * Description: Calculates the employee pay and sets their employee number. 
 * Ryan Guarascia, student number: 000379166, certify that this material is my original work. No other person's work has been used without due acknowledgement and I have not made my work available to anyone else.
 */
package lab3a;

import java.util.Random;

/**
 * Holds all the employee's Information and calculates the employee's pay ad ID
 * number
 *
 * @author rguarascia
 */
public class Employee {

    private static int empCount = 0;
    private String empName;
    private int empNumber;
    private double empSalary;
    private double empRate;
    private double empHours;
    private double empBase;
    private int empPieces;
    private int empType;

    /**
     * Constructor - Sets the Employee's name, increases counter and sets their
     * ID
     *
     * @param name The employee's name.
     */
    public Employee(String name) {
        this.empName = name;
        this.empCount++;
        Random rand = new Random();
        this.empNumber = rand.nextInt((9999 - 1000) + 1) + 1000;
    }

    /**
     * Sets the empType and payment values Salary
     *
     * @param salary The employee's yearly salary
     */
    public void setEmployeePay(double salary) {
        empSalary = salary;
        empType = 1;
    }

    /**
     * Sets the empType and payment values Hourly
     *
     * @param rate The employee's rate per hour
     * @param hours How long the employee worked
     */
    public void setEmployeePay(double rate, double hours) {
        empRate = rate;
        empHours = hours;
        empType = 2;
    }

    /**
     * Sets the empType and payment values Piece work
     *
     * @param base The base pay of the employee no matter what
     * @param pieces How many pieces the employee sold
     */
    public void setEmployeePay(double base, int pieces) {
        empBase = base;
        empPieces = pieces;
        empType = 3;
    }

    /**
     * Gives back how many employees have been entered
     *
     * @return The counter for how many employee's were created
     */
    public static int getCount() {
        return empCount;
    }

    /**
     * Gives back the name of the current employee
     *
     * @return The Employee's name
     */
    public String getName() {
        return empName;
    }

    /**
     * Gives back the empNumber of the current employee
     *
     * @return The Employee's number (ID)
     */
    public int getNumber() {
        return empNumber;
    }

    /**
     * Calculates pays based of the Employee type
     *
     * @return The weekly pay of the Employee
     */
    public double calculatePay() {
        switch (empType) {
            case 1:
                return (double) empSalary / 52;
            case 2:
                if (empHours > 40) {
                    return (40 * empRate + (empHours - 40) * (empRate / 2 + empRate));
                } else {
                    return ((double) empRate * empHours);
                }
            case 3:
                return empPieces * 24 + empBase;
        }
        return 0; //Error Return
    }
}
